import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {

  title:string="black";

  textColor:string = "black";

  color: string = "black";

  stringList: string[] = ['Ioana', 'Alina', 'Cosmin'];
  dateList: Date[] = [new Date("2002-06-06"), new Date("2010-01-16"), new Date("2000-07-10"), new Date("1998-07-16")];

  constructor() { }

  ngOnInit(): void {
  }

  ButtonClick(color: string):void{
    
    this.title = color;
    this.color = color;
  }

}
