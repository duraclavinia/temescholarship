import { Injectable } from '@angular/core';
import { Note } from '../note';
import { FiltersService } from './filters.service';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { map, Observable } from 'rxjs';
import { not } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root'
})

export class NoteService {
  
  readonly baseUrl= "https://localhost:44336/notes";
  
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  // notes: Note[] = [
  //   {
  //     id: "Id1",
  //     title: "First note",
  //     description: "This is the description for the first note",
  //     color:"red",
  //    // category: this._service.getCategories()[0]
  //    categoryID:"1"
  //   },
  //   {
  //     id: "Id2",
  //     title: "Second note",
  //     description: "This is the description for the second note",
  //     color:"blue",
  //     //category: this._service.getCategories()[1]
  //     categoryID:"2"
  //   },
  //   {
  //     id: "Id3",
  //     title: "Third note",
  //     description: "This is the description for the third note",
  //     color:"green",
  //     //category: this._service.getCategories()[2]
  //     categoryID:"3"
  //   },
  //   // {
  //   //   id: "Id4",
  //   //   title: "Fourth note",
  //   //   description: "This is the description for the fourth note"
  //   // }
  // ];

  //constructor(private _service:FiltersService) { }

  constructor(private _httpClient: HttpClient){

  }

  serviceCall() {
    console.log("Note service was called");
  }



  // getNotes(): Note[] {
  //   return this.notes;
  // }

  // add(inputTitle:string, descr:string){
  //   let note: Note;
  //   note.title= inputTitle;
  //   note.description = descr;
  //   this.notes.push(note);
  // }


  // getFiltredNotes(argCategoryID:string){
  //   return this.notes.filter(note => note.categoryID === argCategoryID);
  // }
  

  // getSearchedNotes(searchTerm:string):Note[]{

  //   return this.notes.filter(note=>note.title.toLowerCase().includes(searchTerm.toLowerCase()) || note.description.toLowerCase().includes(searchTerm.toLowerCase()));

  // }
  

  getNotes():Observable<Note[]>{
    return this._httpClient.get<Note[]>(this.baseUrl, this.httpOptions);
  }


  getFilteredNotes(categId: string): Observable<Note[]> {
    return this._httpClient
      .get<Note[]>(
        this.baseUrl,
        this.httpOptions
      )
      .pipe(
        map((notes) => {if(categId === "") {return notes;} return notes.filter((note) => note.categoryId === categId)})
      );
  }



  addNote(note:Note){
    return this._httpClient.post(this.baseUrl , note, this.httpOptions);
  }

  getSearchedNotes(searchTerm:string):Observable<Note[]>{
    return this._httpClient.get<Note[]>(this.baseUrl, this.httpOptions).pipe(map(((notes) => notes.filter(note=>note.title.toLowerCase().includes(searchTerm.toLowerCase()) || note.description.toLowerCase().includes(searchTerm.toLowerCase())))));
  }

  deleteNote(id:string):Observable<Note[]>{
    return this._httpClient.delete<Note[]>(this.baseUrl + id, this.httpOptions);
  }

  editNote(note:Note):Observable<Note[]>{
    return this._httpClient.put<Note[]>(this.baseUrl + '/editnote', note,this.httpOptions);
  }

  getNoteById(id:string):Observable<Note>{
    return this._httpClient.get<Note>(this.baseUrl + '/' +id, this.httpOptions);
  }

}
