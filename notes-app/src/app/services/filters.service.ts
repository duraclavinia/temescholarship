import { Injectable } from '@angular/core';
import { Category } from '../category';

@Injectable({
  providedIn: 'root'
})
export class FiltersService {
  categories: Category[] = [{
    id:'1',
    name:'To Do'
  },
  {
    id:'2',
    name:'Done'
  },
  {
    id:'3',
    name:'Doing'
  }
];

  constructor() { }

  serviceCall() {
    console.log("Note service was called");
  }
  getCategories() {
    return this.categories;
  }
}
