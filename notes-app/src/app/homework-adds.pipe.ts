import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'homeworkAdds'
})
export class HomeworkAddsPipe implements PipeTransform {

  transform(value: string, char: string): string {
    return value = char + value + char;
  }

}
