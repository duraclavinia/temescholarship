import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddNoteComponent } from './add-note/add-note.component';
import { DummyComponentComponent } from './dummy-component/dummy-component.component';
import { EditNoteComponent } from './edit-note/edit-note.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: "add-note", component: AddNoteComponent},
  { path: "add-note/:id", component: AddNoteComponent},
  { path: "", component: HomeComponent, pathMatch:"full" },
  { path:"dummy-component/:id", component: DummyComponentComponent},
  { path:"editnote/:id", component: EditNoteComponent},
  { path: '**', redirectTo: ''}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
