import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Category } from '../category';
import { Note } from '../note';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.scss']
})
export class EditNoteComponent implements OnInit {
  
  private routeSub: Subscription;
  constructor(private route: ActivatedRoute, private _service: NoteService) { }

  note:Note;
  title:string;
  description:string;


  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params =>
       {
         this._service.getNoteById(params['id']).subscribe((response:Note) =>
          {
            this.note = response; 
            console.log(response);
          })
       });
       
  }

  editNote(note:Note){
    this._service.editNote(note).subscribe();
  }
 

}
