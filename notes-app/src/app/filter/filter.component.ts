import { Component, OnInit } from '@angular/core';
import { EventEmitter, Output } from '@angular/core';
import { Category } from '../category';
import { FiltersService } from '../services/filters.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
 
@Output() emitSelectedFilter = new EventEmitter<string>();
@Output() emitSearchFunction = new EventEmitter<string>();
  categories:Category[];
  // categories:Category[] = [
  //   {name:'To Do', id:'1'},
  //   {name:'Done', id:'2'},
  //   {name:'Doing', id:'3'}
  //   ];

  titleSearched:string="";
    
  constructor(private _service:FiltersService) { }

  ngOnInit(): void {
    this._service.serviceCall();
    this.categories = this._service.getCategories();
  }

  selectFilter(categoryId: string){
    this.emitSelectedFilter.emit(categoryId);
  }

  searchNotes(){
    this.emitSearchFunction.emit(this.titleSearched);
  }

}
