import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { Note } from '../note';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit, OnChanges {

  notes: Note[];

  @Input() selectedCategoryId: string;
  @Input() selectedTerm: string;

  constructor(private _router:Router, private service: NoteService) { }

  ngOnInit(): void {
    //this.service.serviceCall();
    //this.notes = this.service.getNotes();
    this.service.getNotes().subscribe((notite: Note[]) => { this.notes = notite; })
  }

  ngOnChanges(): void {
    if(this.selectedCategoryId){
      this.service.getFilteredNotes(this.selectedCategoryId).subscribe((notes: Note[]) => {this.notes = notes; console.log(notes);})
    }
    if(this.selectedTerm){
      this.service.getSearchedNotes(this.selectedTerm).subscribe((notes:Note[]) => {this.notes = notes;})
    }

  }

   editNote(id:string):void{
     this._router.navigateByUrl('/editnote/' + id);
   }

  deleteNote(id:string){
    this.service.deleteNote(id).subscribe();
    this.service.getNotes().subscribe((notes:Note[]) => {this.notes = notes;})
  }

  // logClick() {
  //   alert();
  // }

  // ngOnChanges():void{
  //   if(this.selectedCategoryId){
  //     this.notes = this.service.getFiltredNotes(this.selectedCategoryId);
  //   }
  //   if(this.selectedTerm){
  //     this.notes = this.service.getSearchedNotes(this.selectedTerm);
  //   }
  // }



}
