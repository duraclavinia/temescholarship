import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NoteService } from '../services/note.service';
import { Note } from '../note';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss']
})
export class AddNoteComponent implements OnInit {

  title: string="";
  description: string="";
  addNote:FormGroup;
  categoryId:string;

 // constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

 constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _service:NoteService){}

  ngOnInit(): void {
    // this._activatedRoute.params.subscribe(parameter => {console.log(parameter['id']);  //primul exemplu
    // })

    // this._activatedRoute.queryParams.subscribe(queryParams => {
    //   console.log(queryParams['title']);
    //   console.log(queryParams['description']); //ca sa folosim: localhost:4200/add-note/id?title='myTitle'&description='myDescription'
    // })
    this.addNote = new FormGroup({
      inputTitle: new FormControl(this.title,[Validators.required]),
      descr: new FormControl(this.description,[Validators.required, Validators.minLength(10)])
    });
  }
  
  checkInputs(){
    if(this.title !== "" && this.description !== "" && this.categoryId !== ""){
      return false;
    }
    return true;
  }
  getCategoryIdByName(categId:string){
    switch(categId){
      case "to do":{
        return '1';
      }
      case "doing":{
        return '2';
      }
      case "done":{
        return '3';
      }
      default:
        return null;
    }
  }
  AddNote(){
    const note:Note = {
      title:this.title,
      description:this.description,
      categoryId:this.getCategoryIdByName(this.categoryId),
      color:"yellow"
    }
    console.log(note);
    //this._service.add(this.title,this.description);
    this._service.addNote(note).subscribe();
  }

  get inputNote(){
    return this.addNote.get('inputNote');
  }

  get descr(){
    return this.addNote.get('descr');
  }

 

}
