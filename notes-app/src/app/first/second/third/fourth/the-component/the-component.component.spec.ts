import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TheComponentComponent } from './the-component.component';

describe('TheComponentComponent', () => {
  let component: TheComponentComponent;
  let fixture: ComponentFixture<TheComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TheComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TheComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
