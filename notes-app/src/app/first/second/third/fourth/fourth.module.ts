import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TheComponentComponent } from './the-component/the-component.component';



@NgModule({
  declarations: [
    TheComponentComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    TheComponentComponent
  ]
})
export class FourthModule { }
