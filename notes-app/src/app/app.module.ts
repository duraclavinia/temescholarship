import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DummyModuleModule } from './dummy-module/dummy-module.module';
import { FirstModule } from './first/first.module';
import { SecondModule } from './first/second/second.module';
import { ThirdModule } from './first/second/third/third.module';
import { FourthModule } from './first/second/third/fourth/fourth.module';
import { NoteComponent } from './note/note.component';
import { ToolsComponent } from './tools/tools.component';
import {MatButtonModule } from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import { MatFormFieldModule} from '@angular/material/form-field';
import { FilterComponent } from './filter/filter.component';
import { MatCardModule } from "@angular/material/card";
import { AddValuePipe } from './add-value.pipe';
import { HomeworkAddsPipe } from './homework-adds.pipe';
import { HighlightDirective } from './highlight.directive';
import { AddNoteComponent } from './add-note/add-note.component';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DummyComponentComponent } from './dummy-component/dummy-component.component';
import { NoteService } from './services/note.service';
import { FiltersService } from './services/filters.service';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EditNoteComponent } from './edit-note/edit-note.component';

@NgModule({
  declarations: [
    AppComponent,
    NoteComponent,
    ToolsComponent,
    FilterComponent,
    AddValuePipe,
    HighlightDirective,
    HomeworkAddsPipe,
    AddNoteComponent,
    HomeComponent,
    DummyComponentComponent,//the custom pipe
    EditNoteComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DummyModuleModule,
    FirstModule,
    SecondModule,
    ThirdModule,
    FourthModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule
  ],
  providers: [NoteService, FiltersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
