import { Component, OnInit } from '@angular/core';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  categoryId: string;
  termSearched:string;

  constructor(private _noteService: NoteService) { }

  ngOnInit(): void {
    console.log(this._noteService.getNotes());
  }

  receiveCategory(categId: string) {
    this.categoryId = categId;
    console.log(this.categoryId);
  }
 

  receiveTermSearch(term: string) {
    this.termSearched = term;
  }

}
