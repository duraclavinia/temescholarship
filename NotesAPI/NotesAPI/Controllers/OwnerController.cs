﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{

    [Route("[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {

        IOwnerCollectionService _ownerCollectionService;
        public OwnerController(IOwnerCollectionService ownerCollectionService)
        {
            _ownerCollectionService = ownerCollectionService ?? throw new ArgumentNullException(nameof(ownerCollectionService));
        }
        //static List<Owner> _owners = new List<Owner>()
        //{
        //    new Owner(){Id = new System.Guid(), Name="Lavi"},
        //    new Owner(){Id = new System.Guid(), Name="Ana"},
        //    new Owner(){Id = new System.Guid(), Name="Coco"},
        //};

        /// <summary>
        /// Get all owners
        /// </summary>
        /// <returns></returns>
        
        [HttpGet("/Owners")]
        public async Task<IActionResult> GetAllOwners(){
            return Ok(await _ownerCollectionService.GetAll());
        }


        [HttpPost]
        public async Task<IActionResult> AddOwner([FromBody]Owner owner)
        {
            if (owner == null)
                return BadRequest("Owner is null");
            await _ownerCollectionService.Create(owner);
            return Ok(await _ownerCollectionService.GetAll());
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOwner(Guid id, [FromBody] Owner owner)
        {
            if (owner == null)
                return BadRequest("Owner is null");
            if (!await _ownerCollectionService.Update(id, owner))
            {
                return NotFound("Owner not found");
            }
            return Ok(await _ownerCollectionService.GetAll());
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {

            if (!await _ownerCollectionService.Delete(id))
            {
                return NotFound("Owner not found");
            }
            return Ok("Owner deleted");
        }

        //[HttpGet("ReadOwners")]
        //public IActionResult ReadOwners()
        //{
        //    return Ok(_owners);
        //}

        ///// <summary>
        ///// Add a owner
        ///// </summary>
        ///// <param name="owner"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public IActionResult CreateOwner([FromBody] Owner owner)
        //{
        //    if (owner == null)
        //        return BadRequest("Error! Owner is null");

        //    _ownerCollectionService.Create(owner);
        //    return Ok(_ownerCollectionService.GetAll());
        //}
        //[HttpPost]
        //public IActionResult CreateOwner([FromBody] Owner owner)
        //{
        //    _owners.Add(owner);
        //    return Ok(_owners);
        //}

        ///// <summary>
        ///// update owner based on id
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="owner"></param>
        ///// <returns></returns>
        //[HttpPut("{id}")]
        //public IActionResult UpdateOwner(Guid id, [FromBody] Owner owner)
        //{
        //    if(owner == null)
        //    {
        //        return BadRequest("Note is null");
        //    }

        //    int index = _owners.FindIndex(i => i.Id == id);

        //    if(index == -1 )
        //    {
        //        return NotFound();
        //    }

        //    owner.Id = _owners[index].Id;
        //    _owners[index] = owner;

        //    return Ok();
        //}


        //[HttpPut("{id}")]
        //public IActionResult UpdateOwner(Guid id, [FromBody] Owner owner)
        //{
        //    if (owner == null)
        //    {
        //        return BadRequest("Owner is null");
        //    }

        //    _ownerCollectionService.Update(id, owner);
           
        //    return Ok(_ownerCollectionService.GetAll());
        //}
        ///// <summary>
        ///// delete a owner
        ///// </summary>
        ///// <param name="owner"></param>
        ///// <returns></returns>
        //[HttpDelete]
        //public IActionResult DeleteOwner([FromBody] Owner owner)
        //{
        //    _owners.Remove(owner);
        //    return Ok(_owners);
        //}

        //[HttpDelete]
        //public IActionResult DeleteOwner([FromBody] Owner owner)
        //{

        //    return Ok();
        //}

        ///// <summary>
        ///// delete a owner based on id
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpDelete("{id}")]
        //public IActionResult DeleteOwnerById(Guid id)
        //{
        //    _ownerCollectionService.Delete(id);
        //    return Ok(_ownerCollectionService.GetAll());
        //}
    }
}
