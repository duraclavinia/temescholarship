﻿
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {

        INoteCollectionService _noteCollectionService;
        public NotesController(INoteCollectionService noteCollectionService)
        {
            _noteCollectionService = noteCollectionService ?? throw new ArgumentNullException(nameof(noteCollectionService));
        }


        [HttpGet]
        public async Task<IActionResult> GetNotes()
        {
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetNoteById(Guid id)
        {
            if(id == null)
            {
                return BadRequest("The id is null");
            }
            var note = await _noteCollectionService.Get(id);
            if(note == null)
            {
                return NotFound("Note id not found");
            }
            return Ok(note);
        }

        [HttpPost]
        public async Task<IActionResult> CreateNote([FromBody] Note note)
        {
            if (note == null)
                return BadRequest("Note is null");
            await _noteCollectionService.Create(note);
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpPut("{id}")]
        public async Task<IActionResult>UpdateNote(Guid id, [FromBody]Note note)
        {
            if (note == null)
                return BadRequest("Note is null");
            if(!await _noteCollectionService.Update(id,note))
            {
                return NotFound("Note not found");
            }
            return Ok(await _noteCollectionService.GetAll());
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            
            if (!await _noteCollectionService.Delete(id))
            {
                return NotFound("Note not found");
            }
            return Ok("Note deleted");
        }


        //public NotesController() { }

        ////static List<Note> _notes = new List<Note> { new Note { Id = Guid.NewGuid(), CategoryId = "1", OwnerId = Guid.NewGuid(), Title = "First Note", Description = "First Note Description" },
        ////new Note { Id = Guid.NewGuid(), CategoryId = "1", OwnerId = Guid.NewGuid(), Title = "Second Note", Description = "Second Note Description" },
        ////new Note { Id = Guid.NewGuid(), CategoryId = "1", OwnerId = Guid.NewGuid(), Title = "Third Note", Description = "Third Note Description" },
        ////new Note { Id = Guid.NewGuid(), CategoryId = "1", OwnerId = Guid.NewGuid(), Title = "Fourth Note", Description = "Fourth Note Description" },
        ////new Note { Id = Guid.NewGuid(), CategoryId = "1", OwnerId = Guid.NewGuid(), Title = "Fifth Note", Description = "Fifth Note Description" }
        ////};

        //private static List<Note> _notes = new List<Note> { new Note { Id = new Guid("00000000-0000-0000-0000-000000000001"), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "First Note", Description = "First Note Description" },
        //new Note { Id = new Guid("00000000-0000-0000-0000-000000000002"), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "Second Note", Description = "Second Note Description" },
        //new Note { Id = new Guid("00000000-0000-0000-0000-000000000003"), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "Third Note", Description = "Third Note Description" },
        //new Note { Id = new Guid("00000000-0000-0000-0000-000000000004"), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "Fourth Note", Description = "Fourth Note Description" },
        //new Note { Id = new Guid("00000000-0000-0000-0000-000000000005"), CategoryId = "1", OwnerId = new Guid("00000000-0000-0000-0000-000000000001"), Title = "Fifth Note", Description = "Fifth Note Description" }
        //};


        //[HttpGet]
        //public IActionResult GetNotes()
        //{
        //    List<Note> _notes = _noteCollectionService.GetAll();
        //    return Ok(_notes);
        //}

        //[HttpPost]
        //public IActionResult CreateNotes([FromBody] Note note)
        //{
        //    if (note == null)
        //        return BadRequest("Note is null");
        //    _noteCollectionService.Create(note);
        //    return CreatedAtRoute("GetNoteByNoteId", new { id = note.Id.ToString() }, note);
        //}

        /////// <summary>
        /////// Get a note by owner id
        /////// </summary>
        /////// <param name="id"></param>
        /////// <returns></returns>
        ////[HttpGet("OwnerId/{id}")]
        ////public IActionResult GetNoteByOwnerI(Guid id)
        ////{
        ////    List<Note> notes = _notes.FindAll(note => note.OwnerId == id);
        ////    return Ok(notes);
        ////}

        //[HttpGet("id/{id}")]
        //public IActionResult GetNoteByOwnerI(Guid id)
        //{
        //    List<Note> notes = _noteCollectionService.GetNotesByOwnerId(id);
        //    return Ok(notes);
        //}

        ///// <summary>
        ///// Get a note by note id
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>

        //[HttpGet("{id}", Name = "GetNoteByNoteId")]
        //public IActionResult GetNoteByNoteId(Guid id)
        //{

        //    List<Note> notes = _notes.FindAll(note => note.Id == id);
        //    if (notes == null)
        //    {
        //        return BadRequest("Note not found");
        //    }
        //    return Ok(notes);
        //}

        //[HttpGet("{id}", Name = "GetNoteByNoteId")]
        //public IActionResult GetNoteByNoteId(Guid id)
        //{
        //    return Ok(_noteCollectionService.Get(id));
        //}


        ///// <summary>
        ///// return location header in existing Post response
        ///// </summary>
        ///// <param name="note"></param>
        ///// <returns></returns>
        //[HttpPost("ReturnLocation")]
        //public ActionResult<Note> ReturnLocation([FromBody] Note note)
        //{
        //    if (note == null)
        //    {
        //        return BadRequest("Note was not found");
        //    }

        //    _notes.Add(note);

        //    return CreatedAtRoute("GetNoteByNoteId", new { id = note.Id.ToString() }, note);
        //}


        ///// <summary>
        ///// Update note by id
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="note"></param>
        ///// <returns></returns>
        //[HttpPut("{id}")]
        //public IActionResult UpdateNote(Guid id, [FromBody] Note note)
        //{
        //    if (note == null)
        //        return BadRequest("Note not existing");

        //    int index = _notes.FindIndex(i => i.Id == id);

        //    if (index == -1)
        //    {
        //        return CreateNotes(note);
        //    }

        //    note.Id = id;
        //    _notes[index] = note;

        //    return Ok(_notes);
        //}


        //[HttpPut("{id}")]
        //public IActionResult UpdateNote(Guid id, [FromBody] Note note)
        //{
        //    if (note == null)
        //        return BadRequest("Note not existing");

        //    _noteCollectionService.Update(id, note);

        //    return Ok(_noteCollectionService.GetAll());
        //}

        ///// <summary>
        ///// delete note by id
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpDelete("{id}")]
        //public IActionResult DeleteNote(Guid id)
        //{
        //    int index = _notes.FindIndex(i => i.Id == id);
        //    if(index  == -1)
        //    {
        //        return NotFound("Note not found");
        //    }
        //    _notes.RemoveAt(index);
        //    return Ok();
        //}

        //[HttpDelete("{id}")]
        //public IActionResult DeleteNote(Guid id)
        //{
        //    _noteCollectionService.Delete(id);
        //    return Ok();
        //}

        /////// <summary>
        ///// Update note based on OwnerId and note id.
        ///// </summary>
        ///// <param name="noteId"></param>
        ///// <param name="ownerId"></param>
        ///// <param name="note"></param>
        ///// <returns></returns>
        //[HttpPut("{noteId}/owner")]
        //public IActionResult UpdateNoteByOwnerIdAndNoteId(Guid noteId, Guid ownerId, [FromBody] Note note)
        //{
        //    if (note == null)
        //    {
        //        return BadRequest("Note cannot be null");
        //    }

        //    int index = _notes.FindIndex(i => i.Id == noteId && i.OwnerId == ownerId);

        //    if (index == -1)
        //    {
        //        return NotFound();
        //    }

        //    note.Id = _notes[index].Id;
        //    _notes[index] = note;

        //    return Ok();
        //}


        ///// <summary>
        ///// Delete note based on OwnerId and note id.
        ///// </summary>
        ///// <param name="noteId"></param>
        ///// <param name="ownerId"></param>
        ///// <returns></returns>
        //[HttpDelete("Delete/{noteId}")]
        //public IActionResult DeleteAOwnersNote(Guid noteId, Guid ownerId)
        //{
        //    var index = _notes.FindIndex(i => i.Id == noteId && i.OwnerId == ownerId);

        //    if (index == -1)
        //    {
        //        return NotFound();

        //    }

        //    _notes.RemoveAt(index);

        //    return Ok();
        //}


        ///// <summary>
        ///// Delete all notes of a given OwnerId.
        ///// </summary>
        ///// <param name="ownerId"></param>
        ///// <returns></returns>
        //[HttpDelete("Note/{ownerId}")]
        //public IActionResult DeleteAllNotesByOwnerId(Guid ownerId)
        //{

        //    List<Note> notes = _notes.FindAll(i => i.OwnerId == ownerId);
        //    if (notes == null)
        //    {
        //        return NotFound();

        //    }

        //    _notes.RemoveAll(note => notes.Contains(note));

        //    return Ok();
        //}

        ///// <summary>
        ///// Get all notes
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //public IActionResult Get()
        //{
        //    return Ok("Note controller works");
        //}

        //[HttpGet("{id}")]
        //public IActionResult GetOne(string id)
        //{
        //    return Ok(id);
        //}

        //[HttpPost]
        //public IActionResult Post([FromBody] string note)
        //{
        //    return Ok(note);
        //}

        //[HttpPost]
        //public IActionResult Post2([FromBody] Note note)
        //{
        //    return Ok(note);   // "name" : "value"
        //}




    }




}
