﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private List<Category> categories = new List<Category>()
        {
            new Category(){ Id = Guid.NewGuid(), Name = "To do"},
            new Category(){ Id = Guid.NewGuid(), Name = "Doing"},
            new Category(){ Id = Guid.NewGuid(), Name = "Done"},
        };


        /// <summary>
        /// Returns all categories
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Success</response>
        [HttpGet]
        public IActionResult GetAllCategories()
        {
            return Ok(categories);
        }


        /// <summary>
        /// Get a category by it's id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult GetCategoryById(int id)
        {
            return Ok(categories[id-1]);
        }


        /// <summary>
        ///     Add a category
        /// </summary>
        /// <param name="bodyContent"></param>
        /// <response code="201">Created</response>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult AddCategory([FromBody] Category bodyContent)
        {
            categories.Add(bodyContent);
            return Ok(categories);
        }


        /// <summary>
        ///     Delete a category by the selected id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public List<Category> DeleteCategory(int id)
        {
            categories.Remove(categories[id-1]);
            return categories;
        }

    }
}
