﻿using MongoDB.Driver;
using NotesAPI.Models;
using NotesAPI.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Services
{
    public class OwnerCollectionService : IOwnerCollectionService
    {
        public OwnerCollectionService() { }

        private readonly IMongoCollection<Owner> _owners;

        public OwnerCollectionService(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _owners = database.GetCollection<Owner>(settings.OwnerCollectionName);
        }

        //public List<Owner> _owners = new List<Owner>()
        //{
        //    new Owner(){Id = new System.Guid(), Name="Lavi"},
        //    new Owner(){Id = new System.Guid(), Name="Ana"},
        //    new Owner(){Id = new System.Guid(), Name="Coco"},
        //};

        //public List<Owner> GetAll()
        //{
        //    return _owners;
        //}

        //public Owner Get(Guid id)
        //{
        //    throw new NotImplementedException();
        //}

        //public bool Create(Owner owner)
        //{
        //    _owners.Add(owner);
        //     return true;
        //}

        //public bool Update(Guid id, Owner model)
        //{
        //    int index = _owners.FindIndex(i => i.Id == id);

        //    if (index == -1)
        //        return false;

        //    model.Id = id;
        //    _owners[index] = model;
        //    return true;
        //}

        //public bool Delete(Guid id)
        //{
        //    int index = _owners.FindIndex(i => i.Id == id);

        //    if (index == -1)
        //        return false;

        //    _owners.RemoveAt(index);

        //    return true;
        //}
        public async Task<bool> Create(Owner owner)
        {
            await _owners.InsertOneAsync(owner);
            return true;
        }

        public async Task<bool> Delete(Guid id)
        {
            var result = await _owners.DeleteOneAsync(owner => owner.Id == id);
            if (!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }

        public async Task<Owner> Get(Guid id)
        {
            var result = await _owners.FindAsync(owner => owner.Id == id);

            if (!result.ToList().Any())
            {
                return null;
            }
            return result.ToList()[0];
        }

        public async Task<List<Owner>> GetAll()
        {
            var result = await _owners.FindAsync(owner => true);
            var ownersList = result.ToList();
            return ownersList;
        }

        public async Task<bool> Update(Guid id, Owner owner)
        {
            var result = await _owners.ReplaceOneAsync(owner => owner.Id == id, owner);
            if (!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                await _owners.InsertOneAsync(owner);
                return false;
            }

            return true;
        }

        public Task<List<Note>> GetNotesByOwnerId(Guid ownerId)
        {
            return null;
        }
    }
}
