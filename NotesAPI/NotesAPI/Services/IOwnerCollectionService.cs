﻿using NotesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Services
{
    public interface IOwnerCollectionService : ICollectionService<Owner>
    {
        Task<List<Note>> GetNotesByOwnerId(Guid ownerId);
    }
}
